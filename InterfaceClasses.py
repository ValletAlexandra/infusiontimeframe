from tkinter import *
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.lines as mlines
import matplotlib.widgets as mwidgets
import csv
from scipy.signal import butter, filtfilt
import datetime

matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk


class Interface(Frame):
    
    """Notre fenêtre principale.
    Tous les widgets sont stockés comme attributs de cette fenêtre."""
    
    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre, width=800, height=500, **kwargs)
        self.pack(fill=BOTH)
        self.DB=pd.read_pickle('./Results/InfusionDatabase0')


	#Creation des encarts

        self.frame1=LabelFrame(self, text="Selection fichier", padx=20, pady=20)
        self.frame1.pack(fill="both", expand="yes")

        self.frame1.columnconfigure(0, pad=3)
        self.frame1.columnconfigure(1, pad=3)
        self.frame1.columnconfigure(2, pad=3)
        self.frame1.columnconfigure(3, pad=3)
        self.frame1.columnconfigure(4, pad=3)
        
        self.frame1.rowconfigure(0, pad=3)
        self.frame1.rowconfigure(1, pad=3)

        self.frame2=LabelFrame(self, text="Zones de la courbe", padx=20, pady=20)
        self.frame2.pack(fill="both", expand="yes")

        self.frame2a=Frame(self.frame2, padx=20, pady=20)
        self.frame2a.pack(side=LEFT)

        self.frame2b=Frame(self.frame2, padx=20, pady=20)
        self.frame2b.pack(side=RIGHT)

        self.frame3=LabelFrame(self, text="Début et fin d'injection", padx=20, pady=20)
        self.frame3.pack(fill="both", expand="yes")

        self.frame3a=Frame(self.frame3, padx=20, pady=20)
        self.frame3a.pack(side=LEFT)

        self.frame3b=Frame(self.frame3, padx=20, pady=20)
        self.frame3b.pack(side=RIGHT)

	#Initialisation des variables

        self.DT=50

        self.spantemps=[]
        self.pressure=[]

        self.nopatient = StringVar() 
        self.nopatient.set(str(1))

        self.fichier=StringVar() 
        #file
        dir='./Data/'
        file=self.DB['Fichier'][int(self.nopatient.get())-1][70::]
        self.fichier.set(file)

        self.t1=StringVar() 
        self.t2=StringVar() 

        self.tbasal1=StringVar() 
        self.tbasal1.set(self.DB['ZoneA'][int(self.nopatient.get())-1][0])


        self.tbasal2=StringVar() 
        self.tbasal2.set(self.DB['ZoneA'][int(self.nopatient.get())-1][1])

        self.tmontee1=StringVar() 
        self.tmontee1.set(self.DB['ZoneB'][int(self.nopatient.get())-1][0])

        self.tmontee2=StringVar() 
        self.tmontee2.set(self.DB['ZoneB'][int(self.nopatient.get())-1][1])

        self.tdescente1=StringVar() 
        self.tdescente1.set(self.DB['ZoneC'][int(self.nopatient.get())-1][0])

        self.tdescente2=StringVar() 
        self.tdescente2.set(self.DB['ZoneC'][int(self.nopatient.get())-1][1])

        self.tdeb=StringVar() 
        self.tdeb.set(self.DB['Tdeb'][int(self.nopatient.get())-1])

        self.tfin=StringVar() 
        self.tfin.set(self.DB['Tfin'][int(self.nopatient.get())-1])

        self.Qvalue=StringVar() 
        self.Qvalue.set(self.DB['Qualite'][int(self.nopatient.get())-1])



        self.cutoff = 0.1
        self.fs = 100



        #Frame chargement
        self.message = Label(self.frame1, text="Patient no. : ")
        self.message.grid(row=0, column=0)

        self.entree = Entry(self.frame1, textvariable=self.nopatient, width=30)
        self.entree.grid(row=0, column=1)
        
        self.bouton_moins = Button(self.frame1, text="-", fg="black", command=self.moins)
        self.bouton_moins.grid(row=0, column=2)

        self.bouton_plus = Button(self.frame1, text="+", fg="black", command=self.plus)
        self.bouton_plus.grid(row=0, column=3)

        self.bouton_charger = Button(self.frame1, text="Charger", fg="black", command=self.charger)
        self.bouton_charger.grid(row=0, column=4)

        self.bouton_save = Button(self.frame1, text="Enregistrer", fg="black", command=self.save)
        self.bouton_save.grid(row=1, column=4)

        self.afficheFichier=Label(self.frame1, textvariable=self.fichier)
        self.afficheFichier.grid(row=1,column=0, columnspan=4)

        self.bouton_quitter = Button(self, text="Quitter", command=self.quit)
        self.bouton_quitter.pack(side="bottom")

        # Figure principale
        self.f = plt.Figure(figsize=(5,4), dpi=100)
        self.axe1 = self.f.add_subplot(111)
        self.line1, = self.axe1.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5],'b',alpha=0.2)
        self.line2, = self.axe1.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5],'k-')

        self.rect=patches.Rectangle((0, 0), 0, 0,alpha=0.2)
        self.rect.set_facecolor('none')
        self.rect.set_linewidth('0')
        self.axe1.add_patch(self.rect)

        self.rectbasal = patches.Rectangle((0, 0), 0, 0,alpha=0.2)
        self.rectbasal.set_facecolor('red')
        self.axe1.add_patch(self.rectbasal)

        self.rectmontee = patches.Rectangle((0, 0), 0, 0,alpha=0.2)
        self.rectmontee.set_facecolor('blue')
        self.axe1.add_patch(self.rectmontee)

        self.rectdescente = patches.Rectangle((0, 0), 0, 0,alpha=0.2)
        self.rectdescente.set_facecolor('purple')
        self.axe1.add_patch(self.rectdescente)

        self.currentRect=self.rect
        self.currentT1=self.t1
        self.currentT2=self.t2
        
        self.canvas = FigureCanvasTkAgg(self.f, self.frame2a)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=BOTTOM, fill=BOTH, expand=True)

        self.cursor = mwidgets.Cursor(self.axe1, useblit=True, color='k')
        self.cursor.horizOn = False

        self.lineDeb0 = mlines.Line2D((0,0),(0,0), lw=2.,ls=':', alpha=0.5, color='k')
        self.axe1.add_line(self.lineDeb0)

        self.lineFin0 = mlines.Line2D((0,0),(0,0), lw=2.,ls=':', alpha=0.5, color='k')
        self.axe1.add_line(self.lineFin0)

        aid = self.f.canvas.mpl_connect('button_press_event', self.on_press)
        bid = self.f.canvas.mpl_connect('button_release_event', self.on_release)
        cid = self.f.canvas.mpl_connect('motion_notify_event', self.on_motion)

        # Figures secondaires
        self.f2 = plt.Figure(figsize=(5,4), dpi=100)
        self.axe2 = self.f2.add_subplot(111)
        self.line1a, = self.axe2.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5],'b',alpha=0.2)
        self.line2a, = self.axe2.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5],'k-')

        self.canvas2 = FigureCanvasTkAgg(self.f2, self.frame3a)
        self.canvas2.draw()
        self.canvas2.get_tk_widget().pack(side=BOTTOM, fill=BOTH, expand=True)

        self.cursor2 = mwidgets.Cursor(self.axe2, useblit=True, color='k')
        self.cursor2.horizOn = False

        self.f2.canvas.mpl_connect('button_press_event', self.on_press_tdeb)

        self.lineDeb = mlines.Line2D((0,0),(0,0), lw=2.,ls=':', alpha=0.5, color='k')
        self.axe2.add_line(self.lineDeb)

        self.f3 = plt.Figure(figsize=(5,4), dpi=100)
        self.axe3 = self.f3.add_subplot(111)
        self.line1b, = self.axe3.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5],'b',alpha=0.2)
        self.line2b, = self.axe3.plot([1,2,3,4,5,6,7,8],[5,6,1,3,8,9,3,5],'k-')

        self.canvas3 = FigureCanvasTkAgg(self.f3, self.frame3b)
        self.canvas3.draw()
        self.canvas3.get_tk_widget().pack(side=BOTTOM, fill=BOTH, expand=True)

        self.cursor3 = mwidgets.Cursor(self.axe3, useblit=True, color='k')
        self.cursor3.horizOn = False

        self.f3.canvas.mpl_connect('button_press_event', self.on_press_tfin)

        self.lineFin = mlines.Line2D((0,0),(0,0), lw=2.,ls=':', alpha=0.5,color='k')
        self.axe3.add_line(self.lineFin)

        ## Etat basal
	
        self.frameBasal=LabelFrame(self.frame2b, text="Etat basal", padx=20, pady=20)
        self.frameBasal.pack(side=TOP)

        self.frameBasal.columnconfigure(0, pad=3)
        self.frameBasal.columnconfigure(1, pad=3)
        self.frameBasal.columnconfigure(2, pad=3)
        
        self.frameBasal.rowconfigure(0, pad=3)
        self.frameBasal.rowconfigure(1, pad=3)


        L1 = Label(self.frameBasal, text="t debut : ")
        L1.grid(row=0, column=0,sticky=W)
        self.T1 = Entry(self.frameBasal, textvariable=self.tbasal1, width=30)
        self.T1.grid(row=0, column=1)

        L2 = Label(self.frameBasal, text="t fin : ")
        L2.grid(row=1, column=0,sticky=W)
        self.T2 = Entry(self.frameBasal, textvariable=self.tbasal2, width=30)
        self.T2.grid(row=1, column=1)

        self.bouton_basal = Button(self.frameBasal, text="Modifier", fg="black",
        command=self.selectionbasal)
        self.bouton_basal.grid(row=1, column=2,rowspan=2)

        ## Montee
	
        self.frameMontee=LabelFrame(self.frame2b, text="Injection", padx=20, pady=20)
        self.frameMontee.pack(side=TOP)

        self.frameMontee.columnconfigure(0, pad=3)
        self.frameMontee.columnconfigure(1, pad=3)
        self.frameMontee.columnconfigure(2, pad=3)
        
        self.frameMontee.rowconfigure(0, pad=3)
        self.frameMontee.rowconfigure(1, pad=3)


        L1 = Label(self.frameMontee, text="t debut : ")
        L1.grid(row=0, column=0,sticky=W)
        self.T1 = Entry(self.frameMontee, textvariable=self.tmontee1, width=30)
        self.T1.grid(row=0, column=1)

        L2 = Label(self.frameMontee, text="t fin : ")
        L2.grid(row=1, column=0,sticky=W)
        self.T2 = Entry(self.frameMontee, textvariable=self.tmontee2, width=30)
        self.T2.grid(row=1, column=1)

        self.bouton_montee = Button(self.frameMontee, text="Modifier", fg="black",
        command=self.selectionmontee)
        self.bouton_montee.grid(row=1, column=2,rowspan=2)

        ## Descente
	
        self.frameDescente=LabelFrame(self.frame2b, text="Post injection", padx=20, pady=20)
        self.frameDescente.pack(side=TOP)

        self.frameDescente.columnconfigure(0, pad=3)
        self.frameDescente.columnconfigure(1, pad=3)
        self.frameDescente.columnconfigure(2, pad=3)
        
        self.frameDescente.rowconfigure(0, pad=3)
        self.frameDescente.rowconfigure(1, pad=3)


        L1 = Label(self.frameDescente, text="t debut : ")
        L1.grid(row=0, column=0,sticky=W)
        self.T1 = Entry(self.frameDescente, textvariable=self.tdescente1, width=30)
        self.T1.grid(row=0, column=1)

        L2 = Label(self.frameDescente, text="t fin : ")
        L2.grid(row=1, column=0,sticky=W)
        self.T2 = Entry(self.frameDescente, textvariable=self.tdescente2, width=30)
        self.T2.grid(row=1, column=1)

        self.bouton_descente = Button(self.frameDescente, text="Modifier", fg="black",
        command=self.selectiondescente)
        self.bouton_descente.grid(row=1, column=2,rowspan=2)

        ## Qualite

        self.qualite=Frame(self.frame2b, padx=20, pady=20)
        self.qualite.pack(side=TOP)

        self.qualite.columnconfigure(0, pad=3)
        self.qualite.columnconfigure(1, pad=3)
        self.qualite.columnconfigure(2, pad=3)
        
        self.qualite.rowconfigure(0, pad=3)
        self.qualite.rowconfigure(1, pad=3)

        L7 = Label(self.qualite, text="Type de courbe : ")
        L7.grid(row=0, column=0,sticky=W)
        self.liste = Listbox(self.qualite, bd=1, height=6,selectmode=SINGLE)
        self.liste.insert(1, "Normale")
        self.liste.insert(2, "Norm+regul")
        self.liste.insert(3, "Pathologique")
        self.liste.insert(4, "Patho+regul")
        self.liste.insert(5, "Ondulante")
        self.liste.insert(6, "NC")
        self.liste.select_set(0)
        self.liste.grid(row=0, column=1)

        L8 = Label(self.qualite, text="Qualite : ")
        L8.grid(row=1, column=0,sticky=W)
        self.Q = Entry(self.qualite, textvariable=self.Qvalue, width=30,exportselection=0)
        self.Q.grid(row=1, column=1)

    def plus(self):
          self.nopatient.set(str(int(self.nopatient.get())+1))
          self.charger()

    def moins(self):
          self.nopatient.set(str(int(self.nopatient.get())-1))
          self.charger()	

    
    def charger(self):
          self.nopatient.set(str(int(self.entree.get())))
	  # file to load
          dir='./Data/'
          file=self.DB['Fichier'][int(self.nopatient.get())-1][70::]

          self.fichier.set(dir+file)
          self.tbasal1.set(self.DB['ZoneA'][int(self.nopatient.get())-1][0])
          self.tbasal2.set(self.DB['ZoneA'][int(self.nopatient.get())-1][1])
          self.tmontee1.set(self.DB['ZoneB'][int(self.nopatient.get())-1][0])
          self.tmontee2.set(self.DB['ZoneB'][int(self.nopatient.get())-1][1])
          self.tdescente1.set(self.DB['ZoneC'][int(self.nopatient.get())-1][0])
          self.tdescente2.set(self.DB['ZoneC'][int(self.nopatient.get())-1][1])
          self.tdeb.set(self.DB['Tdeb'][int(self.nopatient.get())-1])
          self.tfin.set(self.DB['Tfin'][int(self.nopatient.get())-1])

          self.Qvalue.set(self.DB['Qualite'][int(self.nopatient.get())-1])

          if (self.liste.curselection()):
              self.liste.select_clear(self.liste.curselection())

          if self.DB['Type'][int(self.nopatient.get())-1]=='N':
              self.liste.select_set(0)
          elif self.DB['Type'][int(self.nopatient.get())-1]=='NR':
              self.liste.select_set(1)
          elif self.DB['Type'][int(self.nopatient.get())-1]=='P':
              self.liste.select_set(2)
          elif self.DB['Type'][int(self.nopatient.get())-1]=='PR':
              self.liste.select_set(3)
          elif self.DB['Type'][int(self.nopatient.get())-1]=='O':
              self.liste.select_set(4)
          else :
              self.liste.select_set(5)

          
          s = open(self.fichier.get()).read().replace(',','.')
          data = np.loadtxt(csv.StringIO(s),skiprows=1,delimiter=';')
          seconds = (data[0,0] - 25569) * 86400.0
          datetime.datetime.utcfromtimestamp(seconds)
          x=[]
          for i in np.arange(len(data[:,0])) :
             seconds = (data[i,0] - 25569) * 86400.0
             date=datetime.datetime.utcfromtimestamp(seconds)
             x.append(date.timestamp())
          tmin=min(np.array(x))
          tmax=max(np.array(x))

          self.spantemps=np.arange(0,tmax-tmin,0.01)
          self.pressure=np.interp(self.spantemps,np.array(x)-tmin,data[:,1])
         
          nyq = 0.5 * self.fs
          normal_cutoff = self.cutoff / nyq
          b, a = butter(5, normal_cutoff, btype='low', analog=False)
          self.pressure_smooth = filtfilt(b, a, self.pressure)


          self.line1.set_data(self.spantemps, self.pressure)
          self.line1a.set_data(self.spantemps, self.pressure)
          self.line1b.set_data(self.spantemps, self.pressure)
          self.line2.set_data(self.spantemps, self.pressure_smooth)
          self.line2a.set_data(self.spantemps, self.pressure_smooth)
          self.line2b.set_data(self.spantemps, self.pressure_smooth)
          self.axe1.axes.set_ylim(0,min(60,max(self.pressure)))
          self.axe1.axes.set_xlim(0,tmax-tmin)


          y0=self.axe1.axes.get_ylim()[0]
          y1=self.axe1.axes.get_ylim()[1]

          print("y0",y0)

          self.rectbasal.set_xy((float(self.tbasal1.get()),y0))
          self.rectbasal.set_width(float(self.tbasal2.get()) - float(self.tbasal1.get()))
          self.rectbasal.set_height(y1 - y0)

          self.rectmontee.set_xy((float(self.tmontee1.get()),y0))
          self.rectmontee.set_width(float(self.tmontee2.get()) - float(self.tmontee1.get()))
          self.rectmontee.set_height(y1 - y0)

          self.rectdescente.set_xy((float(self.tdescente1.get()),y0))
          self.rectdescente.set_width(float(self.tdescente2.get()) - float(self.tdescente1.get()))
          self.rectdescente.set_height(y1 - y0)




          self.lineDeb0.set_data((float(self.tdeb.get()),float(self.tdeb.get())),(self.axe1.axes.get_ylim()[0],self.axe1.axes.get_ylim()[1]))
          self.lineFin0.set_data((float(self.tfin.get()),float(self.tfin.get())),(self.axe1.axes.get_ylim()[0],self.axe1.axes.get_ylim()[1]))

          self.refreshXlim()

          self.lineDeb.set_data((float(self.tdeb.get()),float(self.tdeb.get())),(self.axe2.axes.get_ylim()[0],self.axe2.axes.get_ylim()[1]))
          self.lineFin.set_data((float(self.tfin.get()),float(self.tfin.get())),(self.axe3.axes.get_ylim()[0],self.axe3.axes.get_ylim()[1]))


          self.f.canvas.draw()
          self.f2.canvas.draw()
          self.f3.canvas.draw()

    def save(self):

          self.DB['ZoneA'][int(self.nopatient.get())-1]=(self.tbasal1.get(),self.tbasal2.get())
          self.DB['ZoneB'][int(self.nopatient.get())-1]=(self.tmontee1.get(),self.tmontee2.get())
          self.DB['ZoneC'][int(self.nopatient.get())-1]=(self.tdescente1.get(),self.tdescente2.get())
          self.DB['Tdeb'][int(self.nopatient.get())-1]=self.tdeb.get()
          self.DB['Tfin'][int(self.nopatient.get())-1]=self.tfin.get()

          self.DB['Type'][int(self.nopatient.get())-1]=np.array(['N','NR','P','PR','O','NC'])[self.liste.curselection()[0]]

          self.DB['Qualite'][int(self.nopatient.get())-1]= self.Qvalue.get()

          self.DB.to_pickle('./Results/InfusionDatabase0')

          print('enregistrement de la base de donnée')
          print(self.DB)

    def selectionbasal(self) :
              self.currentRect=self.rectbasal
              self.currentT1=self.tbasal1
              self.currentT2=self.tbasal2
    def selectionmontee(self) :
              self.currentRect=self.rectmontee
              self.currentT1=self.tmontee1
              self.currentT2=self.tmontee2
    def selectiondescente(self) :
              self.currentRect=self.rectdescente
              self.currentT1=self.tdescente1
              self.currentT2=self.tdescente2


    def on_press(self, event):
          self.is_pressed = True
          if event.xdata is not None and event.ydata is not None:
              self.x0= event.xdata
              self.y0=self.axe1.axes.get_ylim()[0]
              print( 'press x0:', self.x0)
              self.currentRect.set_width(0)
              self.currentRect.set_height(0)
              self.currentRect.set_xy((self.x0, self.y0))
              self.f.canvas.draw()


    def on_motion(self, event):
          try:
            if self.is_pressed:
              if event.xdata is not None and event.ydata is not None:
                self.x1 = event.xdata
                self.y1=self.axe1.axes.get_ylim()[1]
                self.currentRect.set_width(self.x1 - self.x0)
                self.currentRect.set_height(self.y1 - self.y0)
                self.currentRect.set_xy((self.x0, self.y0))
                self.f.canvas.draw()
               # print( 'rect:', self.x0, self.y0, self.x1, self.y1, (self.x1-self.x0), (self.y1-self.y0))
          except AttributeError:
            pass 

    def on_release(self, event):
          self.is_pressed = False
          self.x1 = event.xdata
          self.y1=self.axe1.axes.get_ylim()[1]
          self.currentRect.set_width(self.x1 - self.x0)
          self.currentRect.set_height(self.y1 - self.y0)
          self.currentRect.set_xy((self.x0, self.y0))
          print ('unpress x1:',self.x1)
          self.currentT1.set(self.x0)
          self.currentT2.set(self.x1)
          self.f.canvas.draw()
          self.refreshXlim()


    def on_press_tdeb(self, event):
          if event.xdata is not None and event.ydata is not None:
              self.x0= event.xdata
              print( 'press x0:', self.x0)
              self.tdeb.set(self.x0)
              self.lineDeb.set_data((self.x0,self.x0),(self.axe2.axes.get_ylim()[0],self.axe2.axes.get_ylim()[1]))
              self.lineDeb0.set_data((self.x0,self.x0),(self.axe1.axes.get_ylim()[0],self.axe1.axes.get_ylim()[1]))
              self.f.canvas.draw()
              self.f2.canvas.draw()
              self.refreshXlim()

    def on_press_tfin(self, event):
          if event.xdata is not None and event.ydata is not None:
              self.x0= event.xdata
              print( 'press x0:', self.x0)
              self.tfin.set(self.x0)
              self.lineFin.set_data((self.x0,self.x0),(self.axe3.axes.get_ylim()[0],self.axe3.axes.get_ylim()[1]))
              self.lineFin0.set_data((self.x0,self.x0),(self.axe1.axes.get_ylim()[0],self.axe1.axes.get_ylim()[1]))
              self.f.canvas.draw()
              self.f3.canvas.draw()
              self.refreshXlim()

    def refreshXlim(self) :
            if ((self.tdeb.get()>=self.tbasal2.get())*(self.tdeb.get()<=self.tmontee1.get())) :
                  x0=max(float(self.tdeb.get())-float(self.DT),0)
                  x1=float(self.tdeb.get())+2*float(self.DT)            
            else :
                  x0=float(self.tbasal2.get())
                  x1=float(self.tmontee1.get())

            print("x0 x1",x0,x1)

            I=np.where((self.spantemps<x1)*(self.spantemps>x0))
            y0=min(self.pressure[I])
            y1=max(self.pressure[I])
            self.axe2.axes.set_xlim(x0,x1)
            self.axe2.axes.set_ylim(y0,y1)
            self.f2.canvas.draw()

            if ((self.tfin.get()>=self.tmontee2.get())*(self.tfin.get()<=self.tdescente1.get())) :
                  x0=max(float(self.tfin.get())-5*float(self.DT),0)
                  x1=float(self.tfin.get())+5*float(self.DT)            
            else :
                  x0=float(self.tmontee2.get())
                  x1=float(self.tdescente1.get())
            I=np.where((self.spantemps<x1)*(self.spantemps>x0))
            y0=min(self.pressure[I])
            y1=max(self.pressure[I])
            self.axe3.axes.set_xlim(x0,x1)
            self.axe3.axes.set_ylim(y0,y1)
            self.f3.canvas.draw()


